package homeprogramming.mojaprvamatematika.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import homeprogramming.mojaprvamatematika.R;
import homeprogramming.mojaprvamatematika.Start;
import homeprogramming.mojaprvamatematika.baza.Upravljanje_bazom;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB0_9_oduzimanje;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB0_9_zbrajanje;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB10_20_oduzimanje;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB10_20_zbrajanje;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB20_50_oduzimanje;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB20_50_zbrajanje;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB50_100_oduzimanje;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB50_100_zbrajanje;
import homeprogramming.mojaprvamatematika.swipe.OnSwipeTouchListener;

/**
 * Created by Mario on 30.10.2014..
 */
public class O_aplikaciji extends ActionBarActivity {

    private Upravljanje_bazom upravljanje_bazom = new Upravljanje_bazom(this);
    private Intent i;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.o_aplikaciji);
        Aplikacija();
    }

    private void Aplikacija() {

        Context ctx = null;

        final RelativeLayout background = (RelativeLayout) findViewById(R.id.background);

        background.setOnTouchListener(new OnSwipeTouchListener(ctx) {
            public void onSwipeTop() {
                Povratak_u_igru();
            }

            public void onSwipeRight() {

            }

            public void onSwipeLeft() {

            }

            public void onSwipeBottom() {
                Razvojni_tim();
            }

            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.o_aplikaciji_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_novi_igrac:
                Novi_igrac();
                return true;
            case R.id.action_rezultati:
                Rezultati();
                return true;
            case R.id.action_kontakt:
                Razvojni_tim();
                return true;
            case R.id.action_razina:
                Povratak_u_igru();
                return true;
            case R.id.action_exit:
                Izlaz();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void Razvojni_tim() {
        i = new Intent(this, Razvojni_tim.class);
        startActivity(i);
    }

    private void Novi_igrac() {
        i = new Intent(this, Start.class);
        startActivity(i);
    }

    private void Rezultati() {
        i = new Intent(this, Rezultati.class);
        startActivity(i);
    }

    private void Povratak_u_igru() {

        List<String> igraci = new ArrayList<String>();
        String Igrac = String.valueOf(upravljanje_bazom.getZadnjiIgrac()).replace("[", "").replace("]", "").replace(",", "").trim();
        String[] a = Igrac.split(" ");
        for (String sifra : a) {
            igraci.add(sifra);
        }

        String Marker = igraci.get(2);
        int marker = Integer.parseInt(Marker);

        switch (marker) {
            case 1:
                i = new Intent(this, KontrolaGB0_9_oduzimanje.class);
                startActivity(i);
                break;
            case 2:
                i = new Intent(this, KontrolaGB0_9_zbrajanje.class);
                startActivity(i);
                break;
            case 3:
                i = new Intent(this, KontrolaGB10_20_oduzimanje.class);
                startActivity(i);
                break;
            case 4:
                i = new Intent(this, KontrolaGB10_20_zbrajanje.class);
                startActivity(i);
                break;
            case 5:
                i = new Intent(this, KontrolaGB20_50_oduzimanje.class);
                startActivity(i);
                break;
            case 6:
                i = new Intent(this, KontrolaGB20_50_zbrajanje.class);
                startActivity(i);
                break;
            case 7:
                i = new Intent(this, KontrolaGB50_100_oduzimanje.class);
                startActivity(i);
                break;
            case 8:
                i = new Intent(this, KontrolaGB50_100_zbrajanje.class);
                startActivity(i);
                break;
        }

    }

    private void Izlaz() {

        final MediaPlayer mp = MediaPlayer.create(this, R.raw.tick);
        mp.start();

        new AlertDialog.Builder(this)
                .setTitle("Upozorenje!")
                .setMessage("Želiš li zatvoriti aplikaciju?")
                .setPositiveButton("Da", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getApplicationContext(), Start.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("EXIT", true);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Ne", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })
                .setIcon(android.R.drawable.ic_delete)
                .show();
    }

    @Override
    public void onBackPressed() {
        Povratak_u_igru();
    }
}


