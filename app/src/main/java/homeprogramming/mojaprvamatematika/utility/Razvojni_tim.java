package homeprogramming.mojaprvamatematika.utility;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import homeprogramming.mojaprvamatematika.R;
import homeprogramming.mojaprvamatematika.swipe.OnSwipeTouchListener;


/**
 * Created by Mario on 4.11.2014..
 */
public class Razvojni_tim extends ActionBarActivity {

    private String MobProgramer = "+385981384674";
    private String MobDizajner = "+385977131982";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.razvojni_tim);
        Tim();
    }

    public static Intent getOpenFacebookIntentProgramer(Context context) {

        try {
            context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/mario.kusen.3"));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://m.facebook.com/mario.kusen.3"));
        }
    }

    public static Intent getOpenFacebookIntentDizajner(Context context) {

        try {
            context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/ervin.antal?fref=ts"));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://m.facebook.com/ervin.antal?fref=ts"));
        }
    }

    private void Tim() {

        final ImageButton btnMobProgramer = (ImageButton) findViewById(R.id.btnMobProgramer);
        final ImageButton btnEmailProgramer = (ImageButton) findViewById(R.id.btnEmailProgramer);
        final ImageButton btnFacebookProgramer = (ImageButton) findViewById(R.id.btnFacebookProgramer);
        final ImageButton btnMobDizajner = (ImageButton) findViewById(R.id.btnMobDizajner);
        final ImageButton btnEmailDizajner = (ImageButton) findViewById(R.id.btnEmailDizajner);
        final ImageButton btnFacebookDizajner = (ImageButton) findViewById(R.id.btnFacebookDizajner);
        final ImageButton btnFacebookAplikacija = (ImageButton) findViewById(R.id.btnFacebookAplikacija);

        Context ctx = null;

        final RelativeLayout background = (RelativeLayout) findViewById(R.id.background);

        background.setOnTouchListener(new OnSwipeTouchListener(ctx) {
            public void onSwipeTop() {
                O_aplikaciji();
            }

            public void onSwipeRight() {

            }

            public void onSwipeLeft() {

            }

            public void onSwipeBottom() {

            }

            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });

        btnMobProgramer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + Uri.encode(MobProgramer.trim())));
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(callIntent);
                } catch (Exception e) {
                    Toast.makeText(Razvojni_tim.this, "s ovog uređaja nije moguće uspostaviti poziv", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnEmailProgramer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{"kusen.mario@gmail.com"});
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(Razvojni_tim.this, "s ovog uređaja nije moguće poslati e-mail", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnFacebookProgramer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent facebookIntent = getOpenFacebookIntentProgramer(null);
                startActivity(facebookIntent);
            }
        });

        btnMobDizajner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + Uri.encode(MobDizajner.trim())));
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(callIntent);
                } catch (Exception e) {
                    Toast.makeText(Razvojni_tim.this, "s ovog uređaja nije moguće uspostaviti poziv", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnEmailDizajner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{"ervin.antal.1982@gmail.com"});
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(Razvojni_tim.this, "s ovog uređaja nije moguće poslati e-mail", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnFacebookDizajner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent facebookIntent = getOpenFacebookIntentDizajner(null);
                startActivity(facebookIntent);
            }
        });

        btnFacebookAplikacija.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    private void O_aplikaciji() {
        Intent i = new Intent(this, O_aplikaciji.class);
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        O_aplikaciji();
    }
}

