package homeprogramming.mojaprvamatematika.utility;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.Toast;

import homeprogramming.mojaprvamatematika.R;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB0_9_zbrajanje;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB10_20_zbrajanje;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB20_50_zbrajanje;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB50_100_zbrajanje;

/**
 * Created by Mario on 30.10.2014..
 */
public class Razina extends ActionBarActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.odabir_razina);
        Postavke();
    }

    public void Postavke() {

        final CharSequence[] ListaNivoa = {"Razina 1 (0-9)", "Razina 2 (10-20)", "Razina 3 (20-50)", "Razina 4 (50-100)"};
        new AlertDialog.Builder(this)
                .setTitle("Odaberi razinu")
                .setItems(ListaNivoa, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        Toast.makeText(Razina.this, ListaNivoa[item] + " je odabrana", Toast.LENGTH_LONG).show();
                        switch (item) {
                            case 0:
                                pozovi1();
                                break;
                            case 1:
                                pozovi2();
                                break;
                            case 2:
                                pozovi3();
                                break;
                            case 3:
                                pozovi4();
                                break;
                        }
                    }
                })

                .show();
    }

    private void pozovi1() {
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.woosh);
        mp.start();
        Intent i = new Intent(this, KontrolaGB0_9_zbrajanje.class);
        startActivity(i);
    }

    private void pozovi2() {
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.woosh);
        mp.start();
        Intent i = new Intent(this, KontrolaGB10_20_zbrajanje.class);
        startActivity(i);
    }

    private void pozovi3() {
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.woosh);
        mp.start();
        Intent i = new Intent(this, KontrolaGB20_50_zbrajanje.class);
        startActivity(i);
    }

    private void pozovi4() {
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.woosh);
        mp.start();
        Intent i = new Intent(this, KontrolaGB50_100_zbrajanje.class);
        startActivity(i);
    }

}
