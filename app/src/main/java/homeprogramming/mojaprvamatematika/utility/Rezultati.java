package homeprogramming.mojaprvamatematika.utility;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import homeprogramming.mojaprvamatematika.R;
import homeprogramming.mojaprvamatematika.Start;
import homeprogramming.mojaprvamatematika.baza.Upravljanje_bazom;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB0_9_oduzimanje;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB0_9_zbrajanje;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB10_20_oduzimanje;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB10_20_zbrajanje;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB20_50_oduzimanje;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB20_50_zbrajanje;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB50_100_oduzimanje;
import homeprogramming.mojaprvamatematika.kontrola.KontrolaGB50_100_zbrajanje;

/**
 * Created by Mario on 30.10.2014..
 */
public class Rezultati extends ActionBarActivity {

    private ListView listaRezultata;
    private Upravljanje_bazom upravljanje_bazom;
    List<String> sifre;
    List<String> imena;
    ArrayList<String> igraci = new ArrayList<String>();
    ArrayList<ArrayList<Object>> data;
    ArrayAdapter<String> aa;
    private String dataRow;
    private int Sifra;
    private ArrayList<Object> row;
    private int Rezultat1zbrajanje_tocno;
    private int Rezultat2zbrajanje_tocno;
    private int Rezultat3zbrajanje_tocno;
    private int Rezultat4zbrajanje_tocno;
    private int Rezultat1oduzimanje_tocno;
    private int Rezultat2oduzimanje_tocno;
    private int Rezultat3oduzimanje_tocno;
    private int Rezultat4oduzimanje_tocno;
    private int Rezultat1zbrajanje_netocno;
    private int Rezultat2zbrajanje_netocno;
    private int Rezultat3zbrajanje_netocno;
    private int Rezultat4zbrajanje_netocno;
    private int Rezultat1oduzimanje_netocno;
    private int Rezultat2oduzimanje_netocno;
    private int Rezultat3oduzimanje_netocno;
    private int Rezultat4oduzimanje_netocno;
    private String Razina1zbrajanje;
    private String Razina2zbrajanje;
    private String Razina3zbrajanje;
    private String Razina4zbrajanje;
    private String Razina1oduzimanje;
    private String Razina2oduzimanje;
    private String Razina3oduzimanje;
    private String Razina4oduzimanje;
    private Intent i;
    private ImageButton btnZatvoriRezultate;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rezultati);
        IspisiRezultate();
    }

    private void IspisiRezultate() {

        btnZatvoriRezultate = (ImageButton) findViewById(R.id.btnZatvoriRezultate);
        btnZatvoriRezultate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Povratak_u_igru();
            }
        });

        listaRezultata = (ListView) findViewById(R.id.listaRezultata);
        upravljanje_bazom = new Upravljanje_bazom(this);
        try {

            data = upravljanje_bazom.getSviIgraci();
            aa = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, igraci);
            for (int i = 0; i < data.size(); i++) {
                row = data.get(i);
                igraci.add(row.get(0) + " Ime: " + row.get(1).toString() + " " + "(klik za više podataka)");
            }
            listaRezultata.setAdapter(aa);
            listaRezultata.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    dataRow = (String) parent.getItemAtPosition(position);
                    VisePodataka();
                }
            });

        } catch (Exception e) {
            finish();
            Novi_igrac();
        }

    }

    private void VisePodataka() {

        sifre = new ArrayList<String>();
        String[] a = dataRow.split(" ");
        for (String sifra : a) {
            sifre.add(sifra);
        }
        Sifra = Integer.parseInt(sifre.get(0));

        String Igrac = String.valueOf(upravljanje_bazom.DohvatiIgraca(Sifra));

        imena = new ArrayList<String>();
        String[] i = Igrac.split(" ");
        for (String sifra : i) {
            imena.add(sifra);
        }

        String Ime = String.valueOf(imena.get(1).replace(",", ""));

        Rezultat1zbrajanje_tocno = Integer.parseInt(imena.get(2).replace(",", ""));
        Rezultat2zbrajanje_tocno = Integer.parseInt(imena.get(3).replace(",", ""));
        Rezultat3zbrajanje_tocno = Integer.parseInt(imena.get(4).replace(",", ""));
        Rezultat4zbrajanje_tocno = Integer.parseInt(imena.get(5).replace(",", ""));
        Rezultat1oduzimanje_tocno = Integer.parseInt(imena.get(6).replace(",", ""));
        Rezultat2oduzimanje_tocno = Integer.parseInt(imena.get(7).replace(",", ""));
        Rezultat3oduzimanje_tocno = Integer.parseInt(imena.get(8).replace(",", ""));
        Rezultat4oduzimanje_tocno = Integer.parseInt(imena.get(9).replace(",", "").replace("]", ""));

        Rezultat1zbrajanje_netocno = Integer.parseInt(imena.get(10).replace(",", ""));
        Rezultat2zbrajanje_netocno = Integer.parseInt(imena.get(11).replace(",", ""));
        Rezultat3zbrajanje_netocno = Integer.parseInt(imena.get(12).replace(",", ""));
        Rezultat4zbrajanje_netocno = Integer.parseInt(imena.get(13).replace(",", ""));
        Rezultat1oduzimanje_netocno = Integer.parseInt(imena.get(14).replace(",", ""));
        Rezultat2oduzimanje_netocno = Integer.parseInt(imena.get(15).replace(",", ""));
        Rezultat3oduzimanje_netocno = Integer.parseInt(imena.get(16).replace(",", ""));
        Rezultat4oduzimanje_netocno = Integer.parseInt(imena.get(17).replace(",", "").replace("]", ""));

        Razina1zbrajanje = "Točno: " + Rezultat1zbrajanje_tocno + " Netočno: " + Rezultat1zbrajanje_netocno + " Razina 1 zbrajanje";
        Razina2zbrajanje = "Točno: " + Rezultat2zbrajanje_tocno + " Netočno: " + Rezultat2zbrajanje_netocno + " Razina 2 zbrajanje";
        Razina3zbrajanje = "Točno: " + Rezultat3zbrajanje_tocno + " Netočno: " + Rezultat3zbrajanje_netocno + " Razina 3 zbrajanje";
        Razina4zbrajanje = "Točno: " + Rezultat4zbrajanje_tocno + " Netočno: " + Rezultat4zbrajanje_netocno + " Razina 4 zbrajanje";
        Razina1oduzimanje = "Točno: " + Rezultat1oduzimanje_tocno + " Netočno: " + Rezultat1zbrajanje_netocno + " Razina 1 oduzimanje";
        Razina2oduzimanje = "Točno: " + Rezultat2oduzimanje_tocno + " Netočno: " + Rezultat2zbrajanje_netocno + " Razina 2 oduzimanje";
        Razina3oduzimanje = "Točno: " + Rezultat3oduzimanje_tocno + " Netočno: " + Rezultat3zbrajanje_netocno + " Razina 3 oduzimanje";
        Razina4oduzimanje = "Točno: " + Rezultat4oduzimanje_tocno + " Netočno: " + Rezultat4zbrajanje_netocno + " Razina 4 oduzimanje";

        new AlertDialog.Builder(this)
                .setTitle("Ime: " + Ime)
                .setMessage(Razina1zbrajanje + "\n" + Razina2zbrajanje + "\n"
                        + Razina3zbrajanje + "\n" + Razina4zbrajanje + "\n"
                        + Razina1oduzimanje + "\n" + Razina2oduzimanje + "\n"
                        + Razina3oduzimanje + "\n" + Razina4oduzimanje + "\n")
                .setNegativeButton("Zatvori", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        return;
                    }

                })
                .show();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.rezultati_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_razina:
                Povratak_u_igru();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void Povratak_u_igru() {

        List<String> igraci = new ArrayList<String>();
        try {
            String Igrac = String.valueOf(upravljanje_bazom.getZadnjiIgrac()).replace("[", "").replace("]", "").replace(",", "").trim();
            String[] a = Igrac.split(" ");
            for (String sifra : a) {
                igraci.add(sifra);
            }

            String Marker = igraci.get(2);
            int marker = Integer.parseInt(Marker);

            switch (marker) {
                case 1:
                    i = new Intent(this, KontrolaGB0_9_oduzimanje.class);
                    startActivity(i);
                    break;
                case 2:
                    i = new Intent(this, KontrolaGB0_9_zbrajanje.class);
                    startActivity(i);
                    break;
                case 3:
                    i = new Intent(this, KontrolaGB10_20_oduzimanje.class);
                    startActivity(i);
                    break;
                case 4:
                    i = new Intent(this, KontrolaGB10_20_zbrajanje.class);
                    startActivity(i);
                    break;
                case 5:
                    i = new Intent(this, KontrolaGB20_50_oduzimanje.class);
                    startActivity(i);
                    break;
                case 6:
                    i = new Intent(this, KontrolaGB20_50_zbrajanje.class);
                    startActivity(i);
                    break;
                case 7:
                    i = new Intent(this, KontrolaGB50_100_oduzimanje.class);
                    startActivity(i);
                    break;
                case 8:
                    i = new Intent(this, KontrolaGB50_100_zbrajanje.class);
                    startActivity(i);
                    break;
            }

        } catch (Exception e) {
            finish();
            Novi_igrac();
        }

    }

    private void Novi_igrac() {
        i = new Intent(this, Start.class);
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        finish();
        Povratak_u_igru();
    }

}
