package homeprogramming.mojaprvamatematika.baza;

import java.sql.Statement;

/**
 * Created by Mario on 15.11.2014..
 */
public class Baza_igraca {

    String ime;
    int sifra = Statement.RETURN_GENERATED_KEYS;
    int rezultat_tocno;
    int rezultat_netocno;
    int marker;
    int temp_tocno;
    int temp_netocno;

    public Baza_igraca() {

    }

    public Baza_igraca(int sifra, String ime, int rezultat_tocno, int rezultat_netocno, int marker, int temp_tocno, int temp_netocno) {
        this.sifra = sifra;
        this.ime = ime;
        this.rezultat_tocno = rezultat_tocno;
        this.rezultat_netocno = rezultat_netocno;
        this.marker = marker;
        this.temp_tocno = temp_tocno;
        this.temp_netocno = temp_netocno;
    }

    public int getRezultat_netocno() {
        return rezultat_netocno;
    }

    public void setRezultat_netocno(int rezultat_netocno) {
        this.rezultat_netocno = rezultat_netocno;
    }

    public int getMarker() {
        return this.marker;
    }

    public int getTemp_tocno() {
        return temp_tocno;
    }

    public void setTemp_tocno(int temp_tocno) {
        this.temp_tocno = temp_tocno;
    }

    public int getTemp_netocno() {
        return temp_netocno;
    }

    public void setTemp_netocno(int temp_netocno) {
        this.temp_netocno = temp_netocno;
    }

    public void setMarker(int marker) {
        this.marker = marker;
    }

    public int getSifra() {
        return this.sifra;
    }

    public void setSifra(int sifra) {
        this.sifra = sifra;
    }

    public String getIme() {
        return this.ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }


    public int getRezultat_tocno() {
        return rezultat_tocno;
    }

    public void setRezultat_tocno(int rezultat_tocno) {
        this.rezultat_tocno = rezultat_tocno;
    }
}
