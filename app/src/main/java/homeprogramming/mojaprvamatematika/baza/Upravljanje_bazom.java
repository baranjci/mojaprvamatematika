package homeprogramming.mojaprvamatematika.baza;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;

/**
 * Created by Mario on 15.11.2014..
 */
public class Upravljanje_bazom extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "baza_igraca";
    private static final String TABLE_IGRACI = "igraci";
    private static final String COLUMN_SIFRA = "sifra";
    private static final String COLUMN_IME = "ime";

    private static final String COLUMN_ZBRAJANJE_1_TOCNO = "zbrajanje_1_tocno";
    private static final String COLUMN_ZBRAJANJE_2_TOCNO = "zbrajanje_2_tocno";
    private static final String COLUMN_ZBRAJANJE_3_TOCNO = "zbrajanje_3_tocno";
    private static final String COLUMN_ZBRAJANJE_4_TOCNO = "zbrajanje_4_tocno";
    private static final String COLUMN_ODUZIMANJE_1_TOCNO = "oduzimanje_1_tocno";
    private static final String COLUMN_ODUZIMANJE_2_TOCNO = "oduzimanje_2_tocno";
    private static final String COLUMN_ODUZIMANJE_3_TOCNO = "oduzimanje_3_tocno";
    private static final String COLUMN_ODUZIMANJE_4_TOCNO = "oduzimanje_4_tocno";

    private static final String COLUMN_ZBRAJANJE_1_NETOCNO = "zbrajanje_1_netocno";
    private static final String COLUMN_ZBRAJANJE_2_NETOCNO = "zbrajanje_2_netocno";
    private static final String COLUMN_ZBRAJANJE_3_NETOCNO = "zbrajanje_3_netocno";
    private static final String COLUMN_ZBRAJANJE_4_NETOCNO = "zbrajanje_4_netocno";
    private static final String COLUMN_ODUZIMANJE_1_NETOCNO = "oduzimanje_1_netocno";
    private static final String COLUMN_ODUZIMANJE_2_NETOCNO = "oduzimanje_2_netocno";
    private static final String COLUMN_ODUZIMANJE_3_NETOCNO = "oduzimanje_3_netocno";
    private static final String COLUMN_ODUZIMANJE_4_NETOCNO = "oduzimanje_4_netocno";

    private static final String COLUMN_MARKER = "marker";
    private static final String COLUMN_TEMP_TOCNO = "temp_tocno";
    private static final String COLUMN_TEMP_NETOCNO = "temp_netocno";

    public Upravljanje_bazom(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TABLE_IGRACI = "CREATE TABLE " + TABLE_IGRACI + "("

                + COLUMN_SIFRA + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " + COLUMN_IME + " TEXT,"

                + COLUMN_ZBRAJANJE_1_TOCNO + " INTEGER," + COLUMN_ZBRAJANJE_2_TOCNO + " INTEGER," + COLUMN_ZBRAJANJE_3_TOCNO + " INTEGER,"
                + COLUMN_ZBRAJANJE_4_TOCNO + " INTEGER," + COLUMN_ODUZIMANJE_1_TOCNO + " INTEGER," + COLUMN_ODUZIMANJE_2_TOCNO + " INTEGER,"
                + COLUMN_ODUZIMANJE_3_TOCNO + " INTEGER," + COLUMN_ODUZIMANJE_4_TOCNO + " INTEGER," + COLUMN_MARKER + " INTEGER,"

                + COLUMN_ZBRAJANJE_1_NETOCNO + " INTEGER," + COLUMN_ZBRAJANJE_2_NETOCNO + " INTEGER," + COLUMN_ZBRAJANJE_3_NETOCNO + " INTEGER,"
                + COLUMN_ZBRAJANJE_4_NETOCNO + " INTEGER," + COLUMN_ODUZIMANJE_1_NETOCNO + " INTEGER," + COLUMN_ODUZIMANJE_2_NETOCNO + " INTEGER,"
                + COLUMN_ODUZIMANJE_3_NETOCNO + " INTEGER," + COLUMN_ODUZIMANJE_4_NETOCNO + " INTEGER,"

                + COLUMN_TEMP_TOCNO + " INTEGER," + COLUMN_TEMP_NETOCNO + " INTEGER" + ")";

        db.execSQL(CREATE_TABLE_IGRACI);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IGRACI);
        onCreate(db);
    }

    public void dodajIgraca(Baza_igraca baza_igraca) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_IME, baza_igraca.getIme());
        values.put(COLUMN_TEMP_TOCNO, baza_igraca.getTemp_tocno());
        values.put(COLUMN_TEMP_NETOCNO, baza_igraca.getTemp_netocno());
        try {
            db.insert(TABLE_IGRACI, String.valueOf(values), values);
            db.close();
        } catch (Exception e) {
            Log.e("DB ERROR", e.toString());
            e.printStackTrace();
        }
    }

    public int DodajMarker(Baza_igraca igrac) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_MARKER, igrac.getMarker());

        return db.update(TABLE_IGRACI, values, COLUMN_SIFRA + " = ?",
                new String[]{String.valueOf(igrac.getSifra())});

    }

    public int DodajPrivremeniRezultat(Baza_igraca igrac) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_TEMP_TOCNO, igrac.getTemp_tocno());
        values.put(COLUMN_TEMP_NETOCNO, igrac.getTemp_netocno());

        return db.update(TABLE_IGRACI, values, COLUMN_SIFRA + " = ?",
                new String[]{String.valueOf(igrac.getSifra())});
    }

    public void IzbrisiSveIgrace() {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete(TABLE_IGRACI, null, null);
            db.close();
        } catch (Exception e) {
            Log.e("DB ERROR", e.toString());
            e.printStackTrace();
        }
    }

    public ArrayList<ArrayList<Object>> getSviIgraci() {
        ArrayList<ArrayList<Object>> svi_igraci = new ArrayList<ArrayList<Object>>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor;
        cursor = db.query(TABLE_IGRACI, new String[]{COLUMN_SIFRA,
                        COLUMN_IME,
                        COLUMN_ZBRAJANJE_1_TOCNO, COLUMN_ZBRAJANJE_2_TOCNO, COLUMN_ZBRAJANJE_3_TOCNO, COLUMN_ZBRAJANJE_4_TOCNO,
                        COLUMN_ODUZIMANJE_1_TOCNO, COLUMN_ODUZIMANJE_2_TOCNO, COLUMN_ODUZIMANJE_3_TOCNO, COLUMN_ODUZIMANJE_4_TOCNO,

                        COLUMN_ZBRAJANJE_1_NETOCNO, COLUMN_ZBRAJANJE_2_NETOCNO, COLUMN_ZBRAJANJE_3_NETOCNO, COLUMN_ZBRAJANJE_4_NETOCNO,
                        COLUMN_ODUZIMANJE_1_NETOCNO, COLUMN_ODUZIMANJE_2_NETOCNO, COLUMN_ODUZIMANJE_3_NETOCNO, COLUMN_ODUZIMANJE_4_NETOCNO,

                        COLUMN_MARKER},
                null, null, null, null, COLUMN_SIFRA);
        cursor.moveToFirst();
        do {
            ArrayList<Object> igraci = new ArrayList<Object>();
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_SIFRA)));
            igraci.add(cursor.getString(cursor.getColumnIndex(COLUMN_IME)));

            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ZBRAJANJE_1_TOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ZBRAJANJE_2_TOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ZBRAJANJE_3_TOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ZBRAJANJE_4_TOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ODUZIMANJE_1_TOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ODUZIMANJE_2_TOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ODUZIMANJE_3_TOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ODUZIMANJE_4_TOCNO)));

            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ZBRAJANJE_1_NETOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ZBRAJANJE_2_NETOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ZBRAJANJE_3_NETOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ZBRAJANJE_4_NETOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ODUZIMANJE_1_NETOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ODUZIMANJE_2_NETOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ODUZIMANJE_3_NETOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ODUZIMANJE_4_NETOCNO)));

            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_MARKER)));
            svi_igraci.add(igraci);
        } while (cursor.moveToNext());
        db.close();
        return svi_igraci;
    }

    public ArrayList<ArrayList<Object>> getZadnjiIgrac() {
        ArrayList<ArrayList<Object>> zadnji_igrac = new ArrayList<ArrayList<Object>>();
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + " " + TABLE_IGRACI + " " + "ORDER BY" + " " + COLUMN_SIFRA + " " + "DESC LIMIT 1";
        Cursor cursor = db.rawQuery(selectQuery, null);
        try {
            cursor.moveToLast();
            ArrayList<Object> igraci = new ArrayList<Object>();
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_SIFRA)));
            igraci.add(cursor.getString(cursor.getColumnIndex(COLUMN_IME)));
            igraci.add(cursor.getString(cursor.getColumnIndex(COLUMN_MARKER)));
            igraci.add(cursor.getString(cursor.getColumnIndex(COLUMN_TEMP_TOCNO)));
            igraci.add(cursor.getString(cursor.getColumnIndex(COLUMN_TEMP_NETOCNO)));
            zadnji_igrac.add(igraci);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return zadnji_igrac;

    }

    public ArrayList<ArrayList<Object>> DohvatiIgraca(int Sifra) {
        ArrayList<ArrayList<Object>> igrac = new ArrayList<ArrayList<Object>>();
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + " " + TABLE_IGRACI + " " + "WHERE " + COLUMN_SIFRA + " " + "= " + Sifra;
        Cursor cursor = db.rawQuery(selectQuery, null);
        try {
            cursor.moveToLast();
            ArrayList<Object> igraci = new ArrayList<Object>();
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_SIFRA)));
            igraci.add(cursor.getString(cursor.getColumnIndex(COLUMN_IME)));

            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ZBRAJANJE_1_TOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ZBRAJANJE_2_TOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ZBRAJANJE_3_TOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ZBRAJANJE_4_TOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ODUZIMANJE_1_TOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ODUZIMANJE_2_TOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ODUZIMANJE_3_TOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ODUZIMANJE_4_TOCNO)));

            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ZBRAJANJE_1_NETOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ZBRAJANJE_2_NETOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ZBRAJANJE_3_NETOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ZBRAJANJE_4_NETOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ODUZIMANJE_1_NETOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ODUZIMANJE_2_NETOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ODUZIMANJE_3_NETOCNO)));
            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_ODUZIMANJE_4_NETOCNO)));

            igraci.add(cursor.getInt(cursor.getColumnIndex(COLUMN_MARKER)));
            igrac.add(igraci);
            db.close();
        } catch (Exception e) {
        }
        return igrac;
    }


    public int UpdateIgracZbrajanje1(Baza_igraca igrac) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ZBRAJANJE_1_TOCNO, igrac.getRezultat_tocno());
        values.put(COLUMN_ZBRAJANJE_1_NETOCNO, igrac.getRezultat_netocno());
        values.put(COLUMN_TEMP_TOCNO, igrac.getTemp_tocno());
        values.put(COLUMN_TEMP_NETOCNO, igrac.getTemp_netocno());
        return db.update(TABLE_IGRACI, values, COLUMN_SIFRA + " = ?",
                new String[]{String.valueOf(igrac.getSifra())});
    }

    public int UpdateIgracZbrajanje2(Baza_igraca igrac) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ZBRAJANJE_2_TOCNO, igrac.getRezultat_tocno());
        values.put(COLUMN_ZBRAJANJE_2_NETOCNO, igrac.getRezultat_netocno());
        values.put(COLUMN_TEMP_TOCNO, igrac.getTemp_tocno());
        values.put(COLUMN_TEMP_NETOCNO, igrac.getTemp_netocno());
        return db.update(TABLE_IGRACI, values, COLUMN_SIFRA + " = ?",
                new String[]{String.valueOf(igrac.getSifra())});
    }

    public int UpdateIgracZbrajanje3(Baza_igraca igrac) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ZBRAJANJE_3_TOCNO, igrac.getRezultat_tocno());
        values.put(COLUMN_ZBRAJANJE_3_NETOCNO, igrac.getRezultat_netocno());
        values.put(COLUMN_TEMP_TOCNO, igrac.getTemp_tocno());
        values.put(COLUMN_TEMP_NETOCNO, igrac.getTemp_netocno());
        return db.update(TABLE_IGRACI, values, COLUMN_SIFRA + " = ?",
                new String[]{String.valueOf(igrac.getSifra())});
    }

    public int UpdateIgracZbrajanje4(Baza_igraca igrac) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ZBRAJANJE_4_TOCNO, igrac.getRezultat_tocno());
        values.put(COLUMN_ZBRAJANJE_4_NETOCNO, igrac.getRezultat_netocno());
        values.put(COLUMN_TEMP_TOCNO, igrac.getTemp_tocno());
        values.put(COLUMN_TEMP_NETOCNO, igrac.getTemp_netocno());
        return db.update(TABLE_IGRACI, values, COLUMN_SIFRA + " = ?",
                new String[]{String.valueOf(igrac.getSifra())});
    }

    public int UpdateIgracOduzimanje1(Baza_igraca igrac) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ODUZIMANJE_1_TOCNO, igrac.getRezultat_tocno());
        values.put(COLUMN_ODUZIMANJE_1_NETOCNO, igrac.getRezultat_netocno());
        values.put(COLUMN_TEMP_TOCNO, igrac.getTemp_tocno());
        values.put(COLUMN_TEMP_NETOCNO, igrac.getTemp_netocno());
        return db.update(TABLE_IGRACI, values, COLUMN_SIFRA + " = ?",
                new String[]{String.valueOf(igrac.getSifra())});
    }

    public int UpdateIgracOduzimanje2(Baza_igraca igrac) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ODUZIMANJE_2_TOCNO, igrac.getRezultat_tocno());
        values.put(COLUMN_ODUZIMANJE_2_NETOCNO, igrac.getRezultat_netocno());
        values.put(COLUMN_TEMP_TOCNO, igrac.getTemp_tocno());
        values.put(COLUMN_TEMP_NETOCNO, igrac.getTemp_netocno());
        return db.update(TABLE_IGRACI, values, COLUMN_SIFRA + " = ?",
                new String[]{String.valueOf(igrac.getSifra())});
    }

    public int UpdateIgracOduzimanje3(Baza_igraca igrac) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ODUZIMANJE_3_TOCNO, igrac.getRezultat_tocno());
        values.put(COLUMN_ODUZIMANJE_3_NETOCNO, igrac.getRezultat_netocno());
        values.put(COLUMN_TEMP_TOCNO, igrac.getTemp_tocno());
        values.put(COLUMN_TEMP_NETOCNO, igrac.getTemp_netocno());
        return db.update(TABLE_IGRACI, values, COLUMN_SIFRA + " = ?",
                new String[]{String.valueOf(igrac.getSifra())});
    }

    public int UpdateIgracOduzimanje4(Baza_igraca igrac) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ODUZIMANJE_4_TOCNO, igrac.getRezultat_tocno());
        values.put(COLUMN_ODUZIMANJE_4_NETOCNO, igrac.getRezultat_netocno());
        values.put(COLUMN_TEMP_TOCNO, igrac.getTemp_tocno());
        values.put(COLUMN_TEMP_NETOCNO, igrac.getTemp_netocno());
        return db.update(TABLE_IGRACI, values, COLUMN_SIFRA + " = ?",
                new String[]{String.valueOf(igrac.getSifra())});
    }
}
