package homeprogramming.mojaprvamatematika;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import homeprogramming.mojaprvamatematika.baza.Baza_igraca;
import homeprogramming.mojaprvamatematika.baza.Upravljanje_bazom;
import homeprogramming.mojaprvamatematika.kontrola.Kontrola_ime_djeteta;
import homeprogramming.mojaprvamatematika.swipe.OnSwipeTouchListener;
import homeprogramming.mojaprvamatematika.utility.Razina;

public class Start extends ActionBarActivity {

    private Baza_igraca baza_igraca = new Baza_igraca();
    private Upravljanje_bazom upravljanje_bazom = new Upravljanje_bazom(this);
    private int count_tocno = 0;
    private int count_netocno = 0;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start);

        UnosImena();
        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
        }

    }

    private Pattern pattern;
    private Matcher matcher;


    public void UnosImena() {

        final EditText txtImeDjeteta = (EditText) findViewById(R.id.txtImeDjeteta);
        final ImageButton btnDodaj = (ImageButton) findViewById(R.id.btnDodaj);
        final TextView textView = (TextView) findViewById(R.id.textView);

        Context ctx = null;

        final RelativeLayout background = (RelativeLayout) findViewById(R.id.background);

        background.setOnTouchListener(new OnSwipeTouchListener(ctx) {
            public void onSwipeTop() {
                Toast.makeText(Start.this, "Gore nema ništa", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {
                Izlaz();
            }

            public void onSwipeLeft() {
                if (!Kontrola()) {
                    return;
                } else {
                    DodajImeuBazu();
                    Razina();
                    Toast.makeText(Start.this, "Igrač " + txtImeDjeteta.getText() + " je dodan u bazu", Toast.LENGTH_SHORT).show();
                }
            }

            public void onSwipeBottom() {
                Toast.makeText(Start.this, "Dolje nema ništa", Toast.LENGTH_SHORT).show();
            }

            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });

        textView.setText("Upiši ime");
        txtImeDjeteta.setHint("Ime");
        btnDodaj.setEnabled(false);

        btnDodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Kontrola()) {
                    return;
                } else {
                    DodajImeuBazu();
                    Razina();
                    Toast.makeText(Start.this, "Igrač " + txtImeDjeteta.getText() + " je dodan u bazu", Toast.LENGTH_SHORT).show();
                }
            }
        });

        txtImeDjeteta.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!txtImeDjeteta.getText().toString().isEmpty()) {
                    btnDodaj.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void DodajImeuBazu() {
        final EditText txtImeDjeteta = (EditText) findViewById(R.id.txtImeDjeteta);
        String Ime_Djeteta = txtImeDjeteta.getText().toString().trim();
        upravljanje_bazom.dodajIgraca(new Baza_igraca(baza_igraca.getSifra(), Ime_Djeteta, baza_igraca.getRezultat_tocno(),
                baza_igraca.getRezultat_netocno(), baza_igraca.getMarker(), count_tocno, count_netocno));
    }

    private boolean Kontrola() {
        if (!Kontrola_imena()) {
            return false;
        }
        return true;
    }

    private boolean Kontrola_imena() {

        final EditText txtImeDjeteta = (EditText) findViewById(R.id.txtImeDjeteta);
        String Ime_Djeteta = txtImeDjeteta.getText().toString().trim();

        pattern = Pattern.compile(Kontrola_ime_djeteta.IME_PATTERN);
        matcher = pattern.matcher(Ime_Djeteta);

        if (!matcher.matches()) {
            txtImeDjeteta.setBackgroundColor(Color.RED);
            Toast.makeText(Start.this, "Ime nije dobro upisano! Upiši ime ponovno", Toast.LENGTH_LONG).show();
            txtImeDjeteta.setText("");
            return false;
        }
        txtImeDjeteta.setBackgroundColor(Color.TRANSPARENT);
        return true;
    }

    private void Razina() {
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.odabir_razine);
        mp.start();
        Intent i = new Intent(this, Razina.class);
        startActivity(i);

    }

    @Override
    public void onBackPressed() {
        Izlaz();
    }

    private void Izlaz() {

        final MediaPlayer mp = MediaPlayer.create(this, R.raw.tick);
        mp.start();

        new AlertDialog.Builder(this)
                .setTitle("Upozorenje!")
                .setMessage("Želiš li zatvoriti aplikaciju?")
                .setPositiveButton("Da", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getApplicationContext(), Start.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("EXIT", true);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Ne", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })
                .setIcon(android.R.drawable.ic_delete)
                .show();
    }

}
