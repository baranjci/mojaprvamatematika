package homeprogramming.mojaprvamatematika.kontrola;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import homeprogramming.mojaprvamatematika.R;
import homeprogramming.mojaprvamatematika.Start;
import homeprogramming.mojaprvamatematika.baza.Baza_igraca;
import homeprogramming.mojaprvamatematika.baza.Upravljanje_bazom;
import homeprogramming.mojaprvamatematika.generator.GeneratorBrojeva10_20;
import homeprogramming.mojaprvamatematika.swipe.OnSwipeTouchListener;
import homeprogramming.mojaprvamatematika.utility.O_aplikaciji;
import homeprogramming.mojaprvamatematika.utility.Razina;
import homeprogramming.mojaprvamatematika.utility.Razvojni_tim;
import homeprogramming.mojaprvamatematika.utility.Rezultati;

/**
 * Created by Mario on 26.11.2014..
 */
public class KontrolaGB10_20_zbrajanje extends FragmentActivity {

    private GeneratorBrojeva10_20 GB10_20 = new GeneratorBrojeva10_20();
    private Upravljanje_bazom upravljanje_bazom = new Upravljanje_bazom(this);
    private Baza_igraca baza_igraca = new Baza_igraca();
    List<String> igraci;
    private int marker = 4;
    private int count_tocno = 0;
    private int count_netocno = 0;
    private int count_test;
    private int Sifra;
    private String Ime;
    private int temp_tocno;
    private int temp_netocno;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.glavni_zaslon);
        VratiBrojac();
        Razina2zbrajanje();
    }

    private void ProvjeriTest() {

        count_test = count_tocno + count_netocno;

        if (count_test == 5) {
            upravljanje_bazom.UpdateIgracZbrajanje2(new Baza_igraca(Sifra, baza_igraca.getIme(), count_tocno, count_netocno,
                    baza_igraca.getMarker(), 0, 0));
            SetPitanja();
        }

    }

    private void VratiBrojac() {

        igraci = new ArrayList<String>();

        igraci = new ArrayList<String>();

        try {
            String Igrac = String.valueOf(upravljanje_bazom.getZadnjiIgrac()).replace("[", "").replace("]", "").replace(",", "").trim();
            String[] a = Igrac.split(" ");
            for (String sifra : a) {
                igraci.add(sifra);
            }

            String Temp_tocno = igraci.get(3);
            String Temp_netocno = igraci.get(4);

            temp_tocno = Integer.parseInt(Temp_tocno);
            temp_netocno = Integer.parseInt(Temp_netocno);

            if (temp_tocno != 0) {
                count_tocno = temp_tocno;
            }

            if (temp_netocno != 0) {
                count_netocno = temp_netocno;
            }

        } catch (Exception e) {
            Novi_igrac();
        }
    }


    public void Razina2zbrajanje() {

        igraci = new ArrayList<String>();

        try {

            String Igrac = String.valueOf(upravljanje_bazom.getZadnjiIgrac()).replace("[", "").replace("]", "").replace(",", "").trim();
            String[] a = Igrac.split(" ");
            for (String sifra : a) {
                igraci.add(sifra);
            }

            Ime = igraci.get(1);

            final TextView txtRazina = (TextView) findViewById(R.id.txtRazina);
            txtRazina.setText(Ime + " Točno: " + count_tocno + " Netočno: " + count_netocno);

            String s = igraci.get(0);
            Sifra = Integer.parseInt(s);

            upravljanje_bazom.DodajMarker(new Baza_igraca(Sifra, baza_igraca.getIme(), baza_igraca.getRezultat_tocno(),
                    baza_igraca.getRezultat_netocno(), marker, baza_igraca.getTemp_tocno(), baza_igraca.getTemp_netocno()));

        } catch (Exception e) {
            Novi_igrac();
        }


        final ImageButton btnOduzimanje = (ImageButton) findViewById(R.id.btnOduzimanje);
        final ImageButton btnZbrajanje = (ImageButton) findViewById(R.id.btnZbrajanje);
        btnZbrajanje.setVisibility(View.INVISIBLE);
        btnOduzimanje.setVisibility(View.VISIBLE);
        final TextView txtPitanje = (TextView) findViewById(R.id.txtPitanje);
        final ImageButton btnNovaFormula = (ImageButton) findViewById(R.id.btnNovaFormula);
        final ImageButton btnTocno = (ImageButton) findViewById(R.id.btnTocno);
        final ImageButton btnNetocno = (ImageButton) findViewById(R.id.btnNetocno);
        txtPitanje.setText("Je li izraz točan?");

        final TextView txtFormula = (TextView) findViewById(R.id.txtFormula);

        final int A = (GB10_20.a.nextInt(GB10_20.max - GB10_20.min + 1) + GB10_20.min);
        int B = (GB10_20.b.nextInt(GB10_20.max - GB10_20.min + 1) + GB10_20.min);
        int AB_POZ = A + B;
        int C = (GB10_20.c.nextInt(GB10_20.max - GB10_20.min + 1) + GB10_20.min);
        int C1 = C + 1;

        Context ctx = null;

        final RelativeLayout background = (RelativeLayout) findViewById(R.id.background);

        background.setOnTouchListener(new OnSwipeTouchListener(ctx) {
            public void onSwipeTop() {
                upravljanje_bazom.DodajPrivremeniRezultat(new Baza_igraca(Sifra, baza_igraca.getIme(), baza_igraca.getRezultat_tocno(),
                        baza_igraca.getRezultat_netocno(), baza_igraca.getMarker(), count_tocno, count_netocno));
                Rezultati();
            }

            public void onSwipeRight() {
                upravljanje_bazom.DodajPrivremeniRezultat(new Baza_igraca(Sifra, baza_igraca.getIme(), baza_igraca.getRezultat_tocno(),
                        baza_igraca.getRezultat_netocno(), baza_igraca.getMarker(), 0, 0));
                Razina1plus();
            }

            public void onSwipeLeft() {
                upravljanje_bazom.DodajPrivremeniRezultat(new Baza_igraca(Sifra, baza_igraca.getIme(), baza_igraca.getRezultat_tocno(),
                        baza_igraca.getRezultat_netocno(), baza_igraca.getMarker(), 0, 0));
                Razina3plus();
            }

            public void onSwipeBottom() {
                upravljanje_bazom.DodajPrivremeniRezultat(new Baza_igraca(Sifra, baza_igraca.getIme(), baza_igraca.getRezultat_tocno(),
                        baza_igraca.getRezultat_netocno(), baza_igraca.getMarker(), count_tocno, count_netocno));
                OAplikaciji();
            }

            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });

        if (A < 15) {
            txtFormula.setText(A + " + " + B + " = " + AB_POZ);
        } else {
            if (C == AB_POZ) {
                txtFormula.setText(A + " + " + B + " = " + C1);
            } else {
                txtFormula.setText(A + " + " + B + " = " + C);
            }

        }

        btnTocno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (A < 15) {
                    TocanOdgovorZbrajanje2();
                } else {
                    NetocanOdgovor();
                }

            }
        });

        btnNetocno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (A < 15) {
                    NetocanOdgovor();
                } else {
                    NetocnaFormulaZbrajanje();
                }
            }
        });
        btnNovaFormula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Razina2zbrajanje();
            }
        });

        btnOduzimanje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Oduzimanje();
            }
        });


    }

    private void SetPitanja() {
        new AlertDialog.Builder(this)
                .setTitle("Završen je ispit od pet pitanja")
                .setMessage("Točno je odgovoreno na: " + count_tocno + " pitanja\nNetočno je odgovoreno na : " + count_netocno + " pitanja\nPrelaziš na sljedeću razinu")
                .setPositiveButton("Nova razina", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Razina3plus();
                    }
                })

                .setIcon(android.R.drawable.ic_media_ff)
                .show();
    }

    private void NetocanOdgovor() {

        final MediaPlayer mp = MediaPlayer.create(this, R.raw.fart);
        mp.start();

        new AlertDialog.Builder(this)
                .setTitle("Upozorenje")
                .setMessage("Odgovor nije točan! \nPokušaj ponovno")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        count_netocno++;
                        final TextView txtRazina = (TextView) findViewById(R.id.txtRazina);
                        txtRazina.setText(Ime + " Točno: " + count_tocno + " Netočno: " + count_netocno);
                        ProvjeriTest();
                        return;
                    }
                })

                .setIcon(android.R.drawable.ic_delete)
                .show();


    }

    private void TocanOdgovorZbrajanje2() {

        final MediaPlayer mp = MediaPlayer.create(this, R.raw.applause);
        mp.start();

        new AlertDialog.Builder(this)
                .setTitle("Bravo!")
                .setMessage("Odgovor je točan!")
                .setPositiveButton("Novi izraz", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        count_tocno++;
                        final TextView txtRazina = (TextView) findViewById(R.id.txtRazina);
                        txtRazina.setText(Ime + " Točno: " + count_tocno + " Netočno: " + count_netocno);
                        ProvjeriTest();
                        Razina2zbrajanje();
                        mp.stop();
                    }

                })

                .setIcon(android.R.drawable.ic_media_ff)
                .show();

    }

    private void NetocnaFormulaZbrajanje() {

        TextView txtFormula;
        txtFormula = (TextView) findViewById(R.id.txtFormula);

        String F = txtFormula.getText().toString().replace(" + ", " ").replace(" = ", " ");

        List<Integer> integers = new ArrayList<Integer>();
        String[] numbers = F.split(" ");
        for (String number : numbers) {
            integers.add(Integer.valueOf(number.trim()));
        }

        int A = integers.get(0);
        int B = integers.get(1);

        int AB_POZ = A + B;
        int C = (GB10_20.c.nextInt(GB10_20.max - GB10_20.min + 1) + GB10_20.min);
        int C1 = C + 1;

        if (A < 15) {
            new AlertDialog.Builder(this)
                    .setTitle("Bravo!")
                    .setMessage("Rješenje izraza nije točno!\nTočno rješenje izraza " + A + " + " + B + " = " + "je ?")
                    .setPositiveButton("" + AB_POZ, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            TocanOdgovorZbrajanje2();
                        }
                    })
                    .setNegativeButton("" + C, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            NetocanOdgovorZbrajanje();
                        }
                    })
                    .setIcon(android.R.drawable.ic_media_ff)
                    .show();
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("Bravo!")
                    .setMessage("Rješenje izraza nije točno!\nTočno rješenje izraza " + A + " + " + B + " = " + "je ?")
                    .setPositiveButton("" + C, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            NetocanOdgovorZbrajanje();
                        }
                    })
                    .setNegativeButton("" + AB_POZ, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            TocanOdgovorZbrajanje2();
                        }
                    })
                    .setIcon(android.R.drawable.ic_media_ff)
                    .show();
        }

        if (C == AB_POZ) {

            if (A < 15) {
                new AlertDialog.Builder(this)
                        .setTitle("Bravo!")
                        .setMessage("Rješenje izraza nije točno!\nTočno rješenje izraza " + A + " + " + B + " = " + "je ?")
                        .setPositiveButton("" + AB_POZ, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                TocanOdgovorZbrajanje2();
                            }
                        })
                        .setNegativeButton("" + C1, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                NetocanOdgovorZbrajanje();
                            }
                        })
                        .setIcon(android.R.drawable.ic_media_ff)
                        .show();
            } else if (A == 15) {
                new AlertDialog.Builder(this)
                        .setTitle("Bravo!")
                        .setMessage("Rješenje izraza nije točno!\nTočno rješenje izraza " + A + " + " + B + " = " + "je ?")
                        .setPositiveButton("" + C1, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                NetocanOdgovorZbrajanje();
                            }
                        })
                        .setNegativeButton("" + AB_POZ, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                TocanOdgovorZbrajanje2();
                            }
                        })
                        .setIcon(android.R.drawable.ic_media_ff)
                        .show();
            }
        }
    }

    private void NetocanOdgovorZbrajanje() {

        final MediaPlayer mp = MediaPlayer.create(this, R.raw.fart);
        mp.start();

        new AlertDialog.Builder(this)
                .setTitle("Upozorenje")
                .setMessage("Odgovor nije točan! \nPokušaj ponovno")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        count_netocno++;
                        final TextView txtRazina = (TextView) findViewById(R.id.txtRazina);
                        txtRazina.setText(Ime + " Točno: " + count_tocno + " Netočno: " + count_netocno);
                        ProvjeriTest();
                        NetocnaFormulaZbrajanje();
                    }
                })

                .setIcon(android.R.drawable.ic_delete)
                .show();


    }

    private void Oduzimanje() {
        Intent i = new Intent(this, KontrolaGB10_20_oduzimanje.class);
        startActivity(i);
    }

    private void Razina1plus() {
        Intent i = new Intent(this, KontrolaGB0_9_zbrajanje.class);
        startActivity(i);
    }

    private void Razina3plus() {
        Intent i = new Intent(this, KontrolaGB20_50_zbrajanje.class);
        startActivity(i);
    }

    private void Novi_igrac() {
        Intent i = new Intent(this, Start.class);
        startActivity(i);
    }

    private void Rezultati() {
        Intent i = new Intent(this, Rezultati.class);
        startActivity(i);
    }

    private void OAplikaciji() {
        Intent i = new Intent(this, O_aplikaciji.class);
        startActivity(i);
    }

    private void Razina() {
        Intent i = new Intent(this, Razina.class);
        startActivity(i);
    }

    private void RazvojniTim() {
        Intent i = new Intent(this, Razvojni_tim.class);
        startActivity(i);
    }

    private void Izlaz() {

        final MediaPlayer mp = MediaPlayer.create(this, R.raw.tick);
        mp.start();

        new AlertDialog.Builder(this)
                .setTitle("Upozorenje!")
                .setMessage("Želiš li zatvoriti aplikaciju?")
                .setPositiveButton("Da", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getApplicationContext(), Start.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("EXIT", true);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Ne", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })
                .setIcon(android.R.drawable.ic_delete)
                .show();
    }

    @Override
    public void onBackPressed() {
        Izlaz();
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.glavni_zaslon_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_novi_igrac:
                Novi_igrac();
                return true;
            case R.id.action_rezultati:
                upravljanje_bazom.DodajPrivremeniRezultat(new Baza_igraca(Sifra, baza_igraca.getIme(), baza_igraca.getRezultat_tocno(),
                        baza_igraca.getRezultat_netocno(), baza_igraca.getMarker(), count_tocno, count_netocno));
                Rezultati();
                return true;
            case R.id.action_o_aplikaciji:
                upravljanje_bazom.DodajPrivremeniRezultat(new Baza_igraca(Sifra, baza_igraca.getIme(), baza_igraca.getRezultat_tocno(),
                        baza_igraca.getRezultat_netocno(), baza_igraca.getMarker(), count_tocno, count_netocno));
                OAplikaciji();
                return true;
            case R.id.action_razina:
                upravljanje_bazom.DodajPrivremeniRezultat(new Baza_igraca(Sifra, baza_igraca.getIme(), baza_igraca.getRezultat_tocno(),
                        baza_igraca.getRezultat_netocno(), baza_igraca.getMarker(), 0, 0));
                Razina();
                return true;
            case R.id.action_razvojni_tim:
                upravljanje_bazom.DodajPrivremeniRezultat(new Baza_igraca(Sifra, baza_igraca.getIme(), baza_igraca.getRezultat_tocno(),
                        baza_igraca.getRezultat_netocno(), baza_igraca.getMarker(), 0, 0));
                RazvojniTim();
                return true;
            case R.id.action_exit:
                Izlaz();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

